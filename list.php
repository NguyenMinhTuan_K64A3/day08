<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .container {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 34vw;
    margin: 2rem 33vw;
    border: 2px solid #385e8b;
    padding: 1rem;
  }

  .search {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
  }

  .search-child {
    display: flex;
    justify-content: space-around;
    align-items: center;
    margin-bottom: 1rem;
  }

  .search-child-label {
    width: 60px;
  }

  .search-child-select {
    display: flex;
    width: 180px;
    height: 30px;
  }

  .search-child-input {
    display: flex;
    justify-content: space-around;
    justify-content: center;
    width: 200px;
  }

  .hello {
    height: inherit;
    width: inherit;
    border: 2px solid #9cafc6;
  }

  .btn-search {
    display: flex;
    justify-content: center;
  }

  .btn-search1 {
    color: #cfdded;
    background-color: #4f81bd;
    border-radius: 5px;
    border: 2px solid #385e8b;
    height: 30px;
    width: 80px
  }

  .btn-search-remove {
    color: #cfdded;
    background-color: #4f81bd;
    border-radius: 5px;
    border: 2px solid #385e8b;
    height: 30px;
    width: 80px;
    margin-right: 10px;
  }

  .search-result {
    display: flex;
    justify-content: flex-start;
  }

  .btn-add {
    display: flex;
    justify-content: flex-end;
  }

  .btn-add1 {
    color: #cfdded;
    background-color: #4f81bd;
    border-radius: 5px;
    border: 2px solid #385e8b;
    height: 30px;
    width: 80px
  }

  .list {
    display: flex;
    flex-direction: column;
  }

  .list-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 6px;
  }

  .list-item {
    display: flex;
    width: 360px;
    justify-content: space-between;
  }

  .list-item-action {
    display: flex;
    justify-content: flex-end;
  }

  .btn-remove {
    margin-right: 10px;
    height: 30px;
  }

  .btn-patch {
    height: 30px;
  }

  .stt {
    width: 10px;
  }

  .name {
    width: 140px;
  }

  .department {
    width: 140px;
  }
</style>

<body>
  <?php
  session_start();
  $_SESSION['khoa'] = '';
  $_SESSION['word_block'] = '';
  // if (!empty($_POST['btn-add'])) {
  //   header("Location: ./index.php");
  // }
  if (!empty($_POST['btn-search1'])) {
    $_SESSION['khoa'] =  isset($_POST['khoa']) ? $_POST['khoa'] : '';
    $_SESSION['word_block'] = isset($_POST['word-block']) ? $_POST['word-block'] : '';
  }
  if (!empty($_POST['btn-search-remove'])) {
    $_SESSION['khoa'] =  '';
    $_SESSION['word_block'] = '';
  }
  ?>
  <div class="container">
    <form action="" method="POST" id="form" enctype="multipart/form-data">
      <div class="search">
        <div class="search-child">
          <label class="search-child-label">Khoa</label>
          <div class="search-child-select">
            <select class="hello" id="khoa" name="khoa">
              <?php
              $khoa = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
              foreach ($khoa as $key => $value) {
                if ($key == $_SESSION['khoa']) {
                  echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
                } else {
                  echo '<option value="' . $key . '">' . $value . '</option>';
                }
              }
              ?>
            </select>
          </div>
        </div>
        <div class="search-child">
          <label class="search-child-label">Từ khoá</label>
          <div class="search-child-select">
            <?php
            echo '<input class="hello" id="word-block" value="' . $_SESSION['word_block'] . '" name="word-block" type="text" style="padding: 0px" />'
            ?>
          </div>
        </div>
      </div>
      <div class="btn-search">
        <input type="submit" class="btn-search-remove" id="btn-search-remove" onclick="myFunction()" name="btn-search-remove" value="Xoá" />
        <input type="submit" class="btn-search1" name="btn-search1" value="Tìm kiếm" />
      </div>
      <div class="search-result">
        <p>Số sinh viên tìm thấy: XXX</p>
      </div>
      <div class="btn-add">
        <input type="submit" class="btn-add1" name="btn-add" value="Thêm" />
      </div>
      <div class="list">
        <div class="list-header">
          <div class="list-item">
            <p class="stt">NO</p>
            <p class="name">Tên sinh viên</p>
            <p class="department">Khoa</p>
          </div>
          <div class="list-item-action">
            <p>Action</p>
          </div>
        </div>
        <div class="list-header">
          <div class="list-item">
            <p class="stt">1</p>
            <p class="name">Trần Văn A</p>
            <p class="department">Khoa học máy tính</p>
          </div>
          <div class="list-item-action">
            <button class="btn-remove">Xoá</button>
            <button class="btn-patch">Sửa</button>
          </div>
        </div>
        <div class="list-header">
          <div class="list-item">
            <p class="stt">2</p>
            <p class="name">Trần Thị B</p>
            <p class="department">Khoa học máy tính</p>
          </div>
          <div class="list-item-action">
            <button class="btn-remove">Xoá</button>
            <button class="btn-patch">Sửa</button>
          </div>
        </div>
        <div class="list-header">
          <div class="list-item">
            <p class="stt">3</p>
            <p class="name">Nguyễn Hoàng C</p>
            <p class="department">Khoa học vật liệu</p>
          </div>
          <div class="list-item-action">
            <button class="btn-remove">Xoá</button>
            <button class="btn-patch">Sửa</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <script>
    function myFunction() {
      document.getElementById('khoa').value = '';
      document.getElementById('word-block').value = '';
    }
  </script>
</body>

</html>